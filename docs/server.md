# Generating the server

OBA uses [OpenAPI Generator](https://github.com/OpenAPITools/openapi-generator#overview) to generate a server instance.

## OpenAPI Generator

Given an OpenAPI Specification, the OpenAPI Generator generates automatically API client libraries (SDK generation), server stubs, documentation and configuration files. The [OpenAPI Generator website](https://github.com/OpenAPITools/openapi-generator#32---workflow-integration-maven-gradle-github-cicd)
maintains a list of the languages/frameworks that are supported.

### RDF Integration

The OpenAPI Generator provides an API backend based on the OpenAPI Specification. However, it does not provide the logic of the server, like the connection to the target knowledge graph.

OBA modifies the server generated by the OpenAPI Generator using [templates](https://github.com/OpenAPITools/openapi-generator/blob/master/docs/templating.md). As a result, the server can:

 1. Connect and query resources from a target SPARQL endpoint.
    - Convert RDF triples under a given namespace(s) to JSON (using JSON/LD).
 2. Connect and insert resources into the SPARQL endpoint.
    - Convert JSON into RDF triples (using JSON/LD and frames).
    
### Generating JSON/LD contexts

OBA converts the format of the responses from JSON/LD to plain JSON, which is a widely format used by Web developers. In order to achieve this, OBA requires a file with the context. Simply speaking, a context is used to map simple terms to IRIs. 

!!! info 
    See the [JSON/LD documentation](https://json-ld.org/spec/latest/json-ld/#the-context) for more information.

OBA does not currently support the generation of context file. However, this can be easily achieved with [owl2jsonld](https://github.com/sirspock/owl2jsonld).

!!! info
    Our package is a fork of [owl2jsonld](https://github.com/stain/owl2jsonld), developed by [Stain Soiland-Reyes](https://github.com/stain).

#### Generating Contexts with One Ontology

If you have a single ontology, execute the following command:

```bash
java -jar owl2jsonld-0.3.0-SNAPSHOT-standalone.jar  \
    $ONTOLOGY_URL > server/context.json
```

For example, here is how to execute the command using a [sample ontology](https://w3id.org/okn/o/sdm#) with the RDF/XML serialization:

```bash
java -jar owl2jsonld-0.3.0-SNAPSHOT-standalone.jar  \
    https://mintproject.github.io/Mint-ModelCatalog-Ontology/release/1.2.0/ontology.xml > server/context.json
```

#### Generating Contexts with Two or More Ontologies

Since each ontology creates an independent context JSON file, you will need to merge them together. This can be easily achieved by installing  [jq](https://stedolan.github.io/jq/):

```bash

java -jar owl2jsonld-0.3.0-SNAPSHOT-standalone.jar  \
    $ONTOLOGY_URL1 > a.json
java -jar owl2jsonld-0.3.0-SNAPSHOT-standalone.jar \
    $ONTOLOGY_URL2 > b.json    
jq -s '.[0] * .[1]' a.json b.json  | jq -S > context.json
rm a.json b.json

```

For example, in our sample ontology we imported another one:

```bash

java -jar owl2jsonld-0.3.0-SNAPSHOT-standalone.jar  \
    https://mintproject.github.io/Mint-ModelCatalog-Ontology/release/1.2.0/ontology.xml > a.json
java -jar owl2jsonld-0.3.0-SNAPSHOT-standalone.jar \
    https://knowledgecaptureanddiscovery.github.io/SoftwareDescriptionOntology/release/1.2.0/ontology.xml > b.json

jq -s '.[0] * .[1]' a.json b.json  | jq -S > context.json
rm a.json b.json

```

    
## Generating the Server

Currently, OBA supports **Python/Flask** for the server implementation. In order to generate the server, you should follow the instructions below:

### 1 Copying the Context Files

First, you must copy the context file and OpenAPI specification to the right folder:

```bash
mv context.json openapi.yaml python/
```

### 2 Execute Server Generation Scripts

!!! warning
    You must have [Docker](https://docs.docker.com/get-started/) installed.

Access to the directory with the server implementation: 

```bash
$ cd python
```

 
Run the OpenAPI generator script to generate the server:

```bash
$ bash generate-server.sh
...
...
SUCCESS
```

#### Structure of the server

The generated server directory should contain the following files and directories:

```bash
$ ls server/
Dockerfile: A Dockerfile used to build the server Docker image
README.md: A README.md with the instructions to run the server
contexts: Directory with the JSON/LD contexts generated from the ontology
openapi_server: The server implemenation
queries: Directory with the SPARQL queries
requirements.txt: Python requirements of the server 
test-requirements.txt: Python requirements for testing the server 
```


